#pragma once

#include <unordered_map>
#include <functional>

#include "Packet.h"
#include "PacketPlayerLogin.h"

/*workaround class to simulate a static constructor.
In pliststatic's constructor, hardcode idmap to contain all of the packet class's ids and a pointer to a static function
to create them. */
class pliststatic
{
	friend class PacketList;
public:
	pliststatic();
	~pliststatic();
private:
	std::unordered_map<sfn::Int8, std::function<std::shared_ptr<Packet>()>> idmap;
};


/*singleton factory used to construct a packet of a given id type*/
class PacketList
{
public:
	static std::shared_ptr<Packet> ConstructPacket(sfn::Int8 id);
private:
	static pliststatic _stdata;
};

