#pragma once

#include <memory>
#include <vector>
#include <SFNUL.hpp>

#include "Player.h"

typedef sfn::TlsConnection<sfn::TcpSocket, sfn::TlsEndpointType::SERVER, sfn::TlsVerificationType::REQUIRED> TLSConnection;

class Packet
{
public:
	~Packet();

	virtual void ReadFromMessage(std::shared_ptr<sfn::Message> message) = 0;
	virtual void WriteToMessage(std::shared_ptr<sfn::Message> message);

	virtual void ProcessClient();
	virtual void ProcessServer();

	void SetId(sfn::Int8 id);
	const sfn::Int8 GetID() const;

protected:
	Packet();

	sfn::Int8 _id;

	std::shared_ptr<Player> _sender;
	std::vector<Player>* _players;
};

