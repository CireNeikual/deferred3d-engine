#pragma once

#include <SFNUL.hpp>

#include "Packet.h"

class PacketPlayerLogin :
	public Packet
{
public:
	PacketPlayerLogin();
	PacketPlayerLogin(const std::string &username, const std::string &password);
	~PacketPlayerLogin();

	void WriteToMessage(std::shared_ptr<sfn::Message> message) override;
	void ReadFromMessage(std::shared_ptr<sfn::Message> message) override;
	void ProcessServer() override;

	static std::shared_ptr<PacketPlayerLogin> Create();
private:
	std::string _username;
	std::string _password;
};

