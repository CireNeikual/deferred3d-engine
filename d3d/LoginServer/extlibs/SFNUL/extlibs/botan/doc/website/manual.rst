
Reference Manual
========================================

The documentation is all under ``doc`` in your source tree, stored as ``.rst``
(RestructedText) files. They can be processed by Sphinx to produce HTML or
PDF output.

The `manual </manual>`_ and `Doxygen reference </doxygen>`_ for the
latest development release is available online.
