#include "PacketPlayerLogin.h"

PacketPlayerLogin::PacketPlayerLogin()
{
	_id = 1;
}

PacketPlayerLogin::PacketPlayerLogin(const std::string &username, const std::string &password)
{
	_id = 1;
	_username = username;
	_password = password;
}

PacketPlayerLogin::~PacketPlayerLogin()
{
}

std::shared_ptr<PacketPlayerLogin> PacketPlayerLogin::Create()
{
	return std::shared_ptr<PacketPlayerLogin>(new PacketPlayerLogin());
}


void PacketPlayerLogin::WriteToMessage(std::shared_ptr<sfn::Message> message)
{
	(*message) << _username << _password;
}

void PacketPlayerLogin::ReadFromMessage(std::shared_ptr<sfn::Message> message)
{
	(*message) >> _username >> _password;
}

void PacketPlayerLogin::ProcessServer()
{

}