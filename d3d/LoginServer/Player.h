#pragma once

#include <string>
#include <SFNUL.hpp>
#include <botan/botan.h>
#include <botan/bcrypt.h>

#include <sqlite3.h>

typedef sfn::TlsConnection<sfn::TcpSocket, sfn::TlsEndpointType::SERVER, sfn::TlsVerificationType::REQUIRED> TLSConnection;

enum LoginAttemptResult
{
	LOGIN_SUCCESSFUL = 0, //obvious
	LOGIN_FAILED_INCORRECT_USERNAME, //username does not exist in the database
	LOGIN_FAILED_INVALID_USERNAME_FORMAT, //username contains invalid characters
	LOGIN_FAILED_INCORRECT_PASSWORD, //password given does not match with the hash in the database.
	LOGIN_FAILED_CONNECTION_ISSUE //couldn't access the database for some reason.
};

class Player
{
public:
	Player(const std::shared_ptr<TLSConnection> &connection);
	~Player();
	LoginAttemptResult LoadFromMySql(sqlite3* database, const std::string &username, const std::string &password);
	std::string Register(sqlite3* database, const std::string &username, const std::string &password);
private:
	static sqlite3_stmt* _loginstatement;

	std::string _username;
	std::string _passwordhash;

	std::shared_ptr<TLSConnection> _connection;
	
	static bool VerifyUsernameIsValid(const std::string &username);
};