#include <sceneobjects/SceneObjectEntryPoint.h>

#include <sceneobjects/SceneObjectProp.h>
#include <sceneobjects/SceneObjectPlayer.h>
#include <sceneobjects/SceneObjectBox.h>
#include <sceneobjects/gui/SceneObjectGUI.h>
#include <sceneobjects/gui/SceneObjectMousePointer.h>
#include <sceneobjects/map/Map3DWS.h>
#include <sceneobjects/trixel/TrileMap.h>
#include <sceneobjects/trixel/TrileExplosion.h>

#include <rendering/voxel/VoxelTerrain.h>
#include <rendering/voxel/TerrainGenerator.h>
#include <rendering/lighting/SceneObjectDirectionalLightShadowed.h>
#include <rendering/lighting/SceneObjectPointLightShadowed.h>

#include <rendering/imageeffects/SceneObjectEffectBuffer.h>
#include <rendering/imageeffects/SceneObjectSSAO.h>
#include <rendering/imageeffects/SceneObjectLightScattering.h>
#include <rendering/imageeffects/SceneObjectDepthOfField.h>
#include <rendering/imageeffects/SceneObjectSSR.h>
#include <rendering/imageeffects/SceneObjectFXAA.h>
#include <rendering/imageeffects/SceneObjectFog.h>
#include <rendering/imageeffects/SceneObjectGI.h>

#include <sceneobjects/SceneObjectFloatingCamera.h>

#include <system/SoftwareImage2D.h>

void SceneObjectEntryPoint::onAdd() {
	// Physics

	std::shared_ptr<d3d::SceneObjectPhysicsWorld> physicsWorld(new d3d::SceneObjectPhysicsWorld());

	getRenderScene()->addNamed(physicsWorld, "physWrld");

	d3d::SceneObjectRef lighting = getScene()->getNamed("lighting");

	d3d::SceneObjectLighting* pLighting = static_cast<d3d::SceneObjectLighting*>(lighting.get());

	pLighting->_ambientLight = d3d::Vec3f(0.01f, 0.01f, 0.01f);

	/*std::shared_ptr<d3d::SceneObjectDirectionalLightShadowed> light(new d3d::SceneObjectDirectionalLightShadowed());

	getRenderScene()->add(light);

	light->create(pLighting, 2, 2048, 0.5f, 100.0f, 0.6f);

	light->setDirection(d3d::Vec3f(-0.4f, -1.0f, 0.6f).normalized());

	light->setColor(d3d::Vec3f(1.0f, 1.0f, 1.0f));*/

	// GUI

	/*std::shared_ptr<SceneObjectGUI> gui(new SceneObjectGUI());

	getRenderScene()->addNamed(gui, "gui", false);

	gui->_layer = 2.0f;*/

	// Control

	std::shared_ptr<SceneObjectFloatingCamera> camera(new SceneObjectFloatingCamera());

	getRenderScene()->add(camera, false);

	// Map

	/*std::shared_ptr<d3d::Map3DWS> map(new d3d::Map3DWS());

	map->_settings._pScene = getScene();

	map->createAsset("resources/maps/horrorMap.3dw");

	d3d::addMapLights(*map, getScene());*/

	std::shared_ptr<SceneObjectProp> prop(new SceneObjectProp());

	getScene()->add(prop, true);

	prop->create("resources/models/GITest3.obj");

	prop->calculateAABB();

	std::shared_ptr<d3d::SceneObjectPointLightShadowed> pointLight(new d3d::SceneObjectPointLightShadowed());

	getScene()->add(pointLight);

	pointLight->create(pLighting, 128);

	pointLight->setRange(4.0f);

	pointLight->setPosition(d3d::Vec3f(0.0f, 2.0f, 0.0f));

	// ------------------------------------------- Image Effects -------------------------------------------

	std::shared_ptr<d3d::SceneObjectEffectBuffer> effectBuffer(new d3d::SceneObjectEffectBuffer());

	getRenderScene()->addNamed(effectBuffer, "ebuf", false);

	effectBuffer->create(0.5f);

	std::shared_ptr<d3d::Asset> assetBlurHorizontal;

	getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/blurHorizontal.frag", assetBlurHorizontal);

	std::shared_ptr<d3d::Shader> blurShaderHorizontal = std::static_pointer_cast<d3d::Shader>(assetBlurHorizontal);

	std::shared_ptr<d3d::Asset> assetBlurVertical;

	getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/blurVertical.frag", assetBlurVertical);

	std::shared_ptr<d3d::Shader> blurShaderVertical = std::static_pointer_cast<d3d::Shader>(assetBlurVertical);

	std::shared_ptr<d3d::Asset> assetRenderImage;

	getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/renderImage.frag", assetRenderImage);

	std::shared_ptr<d3d::Shader> renderImageShader = std::static_pointer_cast<d3d::Shader>(assetRenderImage);

	std::shared_ptr<d3d::Asset> assetRenderMultImage;

	getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/renderImageMult.frag", assetRenderMultImage);

	std::shared_ptr<d3d::Shader> renderImageMultShader = std::static_pointer_cast<d3d::Shader>(assetRenderMultImage);

	std::shared_ptr<d3d::Asset> assetNoise;

	getScene()->getAssetManager("tex2D", d3d::Texture2D::assetFactory)->getAsset("resources/textures/noise.bmp", assetNoise);

	std::shared_ptr<d3d::Texture2D> noiseMap = std::static_pointer_cast<d3d::Texture2D>(assetNoise);

	noiseMap->bind();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	std::shared_ptr<d3d::Asset> assetBlurHorizontalEdgeAware;

	getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/blurHorizontalEdgeAware.frag", assetBlurHorizontalEdgeAware);

	std::shared_ptr<d3d::Shader> blurShaderHorizontalEdgeAware = std::static_pointer_cast<d3d::Shader>(assetBlurHorizontalEdgeAware);

	std::shared_ptr<d3d::Asset> assetBlurVerticalEdgeAware;

	getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/blurVerticalEdgeAware.frag", assetBlurVerticalEdgeAware);

	std::shared_ptr<d3d::Shader> blurShaderVerticalEdgeAware = std::static_pointer_cast<d3d::Shader>(assetBlurVerticalEdgeAware);

	// SSAO

	/*std::shared_ptr<d3d::Shader> ssaoShader(new d3d::Shader());

	ssaoShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/ssao.frag");

	std::shared_ptr<d3d::SceneObjectSSAO> ssao(new d3d::SceneObjectSSAO());

	getRenderScene()->add(ssao, false);

	ssao->create(blurShaderHorizontal, blurShaderVertical, ssaoShader, renderImageShader, noiseMap);

	ssao->_ssaoRadius = 0.5f;
	ssao->_ssaoStrength = 4.0f;
	ssao->_blurRadius = 0.002f;
	ssao->_numBlurPasses = 1;*/

	// GI

	std::shared_ptr<d3d::Asset> assetGIShader;

	getScene()->getAssetManager("shader", d3d::Shader::assetFactory)->getAsset("COMPUTE resources/shaders/imageeffects/gi.comp", assetGIShader);

	std::shared_ptr<d3d::Shader> giShader = std::static_pointer_cast<d3d::Shader>(assetGIShader);

	std::shared_ptr<d3d::SceneObjectGI> gi(new d3d::SceneObjectGI());

	getScene()->add(gi, false);

	gi->create(blurShaderHorizontalEdgeAware, blurShaderVerticalEdgeAware, renderImageShader, renderImageMultShader, giShader, noiseMap, 128, 128, 0.25f);

	gi->clearGeometry(prop->getAABB());
	gi->clearMaterials();
	gi->clearLights();

	gi->addModel(prop->getModel()->_model, d3d::Matrix4x4f::identityMatrix());

	gi->addLight(pointLight);

	gi->compileGeometry();
	gi->compileMaterials();
	gi->compileLights();

	// SSR

	/*std::shared_ptr<d3d::TextureCube> cubeMap(new d3d::TextureCube());

	cubeMap->createAsset(
		"resources/environmentmaps/envMap0.png "
		"resources/environmentmaps/envMap1.png "
		"resources/environmentmaps/envMap2.png "
		"resources/environmentmaps/envMap3.png "
		"resources/environmentmaps/envMap4.png "
		"resources/environmentmaps/envMap5.png");

	std::shared_ptr<d3d::Shader> ssrShader(new d3d::Shader());

	ssrShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/ssr.frag");

	std::shared_ptr<d3d::SceneObjectSSR> ssr(new d3d::SceneObjectSSR());

	getRenderScene()->add(ssr, false);

	ssr->create(blurShaderHorizontalEdgeAware, blurShaderVerticalEdgeAware, ssrShader, renderImageShader, cubeMap, noiseMap);

	ssr->_layer = 1.0f;*/

	// Light Scattering

	/*std::shared_ptr<d3d::Shader> lightScatteringShader(new d3d::Shader());

	lightScatteringShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/lightScattering.frag");

	std::shared_ptr<d3d::SceneObjectLightScattering> lightScattering(new d3d::SceneObjectLightScattering());

	getRenderScene()->add(lightScattering, false);

	lightScattering->create(blurShaderHorizontal, blurShaderVertical, lightScatteringShader, renderImageShader);

	lightScattering->_layer = 1.5f;

	lightScattering->_lightSourcePosition = -light->getDirection() * 200.0f;
	lightScattering->_lightSourceColor = d3d::Vec3f(1.0f, 0.9f, 0.8f) * 0.5f;*/

	// Depth of field

	/*std::shared_ptr<d3d::Shader> depthOfFieldBlurShaderHorizontal(new d3d::Shader());

	depthOfFieldBlurShaderHorizontal->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/depthOfFieldBlurHorizontal.frag");

	std::shared_ptr<d3d::Shader> depthOfFieldBlurShaderVertical(new d3d::Shader());

	depthOfFieldBlurShaderVertical->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/depthOfFieldBlurVertical.frag");

	std::shared_ptr<d3d::SceneObjectDepthOfField> depthOfField(new d3d::SceneObjectDepthOfField());

	getRenderScene()->add(depthOfField, false);

	depthOfField->create(depthOfFieldBlurShaderHorizontal, depthOfFieldBlurShaderVertical, renderImageShader);

	depthOfField->_layer = 1.5f;

	depthOfField->_focalDistance = 9.0f;
	depthOfField->_focalRange = 0.4f;
	depthOfField->_blurRadius = 0.002f;
	depthOfField->_numBlurPasses = 1;*/

	// FXAA

	std::shared_ptr<d3d::Shader> lumaShader(new d3d::Shader());

	lumaShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/lumaRender.frag");

	std::shared_ptr<d3d::Shader> fxaaShader(new d3d::Shader());

	fxaaShader->createAsset("NONE resources/shaders/noTransformVertex.vert resources/shaders/imageeffects/fxaa.frag");

	std::shared_ptr<d3d::SceneObjectFXAA> fxaa(new d3d::SceneObjectFXAA());

	getRenderScene()->add(fxaa, false);

	fxaa->create(fxaaShader, lumaShader);

	destroy();
}