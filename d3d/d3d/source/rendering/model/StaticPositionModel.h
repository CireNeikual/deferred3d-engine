#pragma once

#include <rendering/mesh/StaticPositionMesh.h>
#include <rendering/material/Material.h>

#include <scene/RenderScene.h>

namespace d3d {
	class StaticPositionModel {
	public:
		std::vector<std::shared_ptr<StaticPositionMesh>> _meshes;

		void render();

		// Loaders
		bool loadFromOBJ(const std::string &fileName, AABB3D &aabb, bool useBuffers, bool clearArrays);
	};
}