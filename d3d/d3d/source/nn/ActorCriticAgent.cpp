/*
NN
Copyright (C) 2013 Eric Laukien

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#include <nn/ActorCriticAgent.h>
#include <iostream>

using namespace nn;

ActorCriticAgent::ActorCriticAgent()
: _prevValue(0.0f),
_actorAlpha(0.1f), _criticAlpha(0.01f), _criticDecay(0.0f),
_lambda(0.75f), _gamma(0.95f), _minError(0.001f),
_numBackpropPasses(4), _criticMomentum(0.0f),
_criticErrorTolerance(0.05f),
_negativeErrorMultiplier(1.5f),
_numPseudoRehearsalSamplesCritic(0),
_pseudoRehearsalSampleStdDev(0.75f),
_pseudoRehearsalSampleMean(0.0f),
_maxEligibilityTraceChainSize(60)
{}

void ActorCriticAgent::createRandom(size_t numInputs, size_t numOutputs,
	size_t numActorHiddenLayers, size_t numActorNeuronsPerHiddenLayer,
	size_t numCriticHiddenLayers, size_t numCriticNeuronsPerHiddenLayer,
	float minWeight, float maxWeight, unsigned long seed)
{
	_actor.createRandom(numInputs, numOutputs, numActorHiddenLayers, numActorNeuronsPerHiddenLayer, minWeight, maxWeight, seed);
	_critic.createRandom(numInputs, 1, numCriticHiddenLayers, numCriticNeuronsPerHiddenLayer, minWeight, maxWeight, seed + 1);

	_prevInputs.assign(numInputs, 0.0f);
}

void ActorCriticAgent::step(float fitness) {
	std::vector<float> currentInputs(getNumInputs());

	for (size_t i = 0; i < getNumInputs(); i++) {
		currentInputs[i] = _actor.getInput(i);
		_critic.setInput(i, _actor.getInput(i));
	}

	_critic.activateLinearOutputLayer();

	float value = _critic.getOutput(0);

	// Gather rehearsal samples
	std::vector<IOSet> criticRehearsalSamples(_numPseudoRehearsalSamplesCritic);

	std::normal_distribution<float> pseudoRehearsalInputDistribution(_pseudoRehearsalSampleMean, _pseudoRehearsalSampleStdDev);

	for (size_t i = 0; i < _numPseudoRehearsalSamplesCritic; i++) {
		// Generate critic sample
		criticRehearsalSamples[i]._inputs.resize(_critic.getNumInputs());

		for (size_t j = 0; j < _actor.getNumInputs(); j++) {
			criticRehearsalSamples[i]._inputs[j] = pseudoRehearsalInputDistribution(_generator);
			_critic.setInput(j, criticRehearsalSamples[i]._inputs[j]);
		}

		_critic.activate();

		criticRehearsalSamples[i]._outputs.resize(_critic.getNumOutputs());

		for (size_t j = 0; j < _critic.getNumOutputs(); j++)
			criticRehearsalSamples[i]._outputs[j] = _critic.getOutput(j);
	}

	float newPrevValue = fitness + _gamma * value;
	float error = newPrevValue - _prevValue;
	float criticTarget = _prevValue + _lambda * error;

	// Add discounted reward to eligibility traces. Propagate reward downward
	float prevChainValue = criticTarget;

	for (std::list<EligibilityTrace>::iterator it = _eligibilityTraceChain.begin(); it != _eligibilityTraceChain.end(); it++) {
		it->_value = (1.0f - _lambda) * it->_value + _lambda * (it->_fitness + _gamma * prevChainValue);
		prevChainValue = it->_value;
	}

	// Track square error for early stopping
	float squareError = 99999.0f;

	for (size_t p = 0; p < _numBackpropPasses && squareError > _criticErrorTolerance; p++) {
		float error;

		// Recalculate sum of square errors
		squareError = 0.0f;

		// Train on new sample
		for (size_t i = 0; i < _prevInputs.size(); i++)
			_critic.setInput(i, _prevInputs[i]);

		_critic.activateLinearOutputLayer();

		error = criticTarget - _critic.getOutput(0);

		squareError += error * error;

		FeedForwardNeuralNetwork::Gradient gradient;
		_critic.getGradientLinearOutputLayer(std::vector<float>(1, criticTarget), gradient);
		_critic.moveAlongGradientMomentum(gradient, _criticAlpha, _criticMomentum);

		// Keep value for next state
		/*for (size_t i = 0; i < currentInputs.size(); i++)
		_critic.setInput(i, currentInputs[i]);

		_critic.activateLinearOutputLayer();

		_critic.getGradientLinearOutputLayer(std::vector<float>(1, value), gradient);
		_critic.moveAlongGradientMomentum(gradient, _criticAlpha, _criticMomentum);*/

		// Train on previous samples
		for (std::list<EligibilityTrace>::iterator it = _eligibilityTraceChain.begin(); it != _eligibilityTraceChain.end(); it++) {
			for (size_t i = 0; i < it->_inputs.size(); i++)
				_critic.setInput(i, it->_inputs[i]);

			_critic.activateLinearOutputLayer();

			error = it->_value - _critic.getOutput(0);

			squareError += error * error;

			FeedForwardNeuralNetwork::Gradient gradient;
			_critic.getGradientLinearOutputLayer(std::vector<float>(1, it->_value), gradient);
			_critic.moveAlongGradientMomentum(gradient, _criticAlpha, _criticMomentum);
		}

		// Train on rehearsal samples
		for (size_t i = 0; i < criticRehearsalSamples.size(); i++) {
			for (size_t j = 0; j < _critic.getNumInputs(); j++)
				_critic.setInput(j, criticRehearsalSamples[i]._inputs[j]);

			_critic.activateLinearOutputLayer();

			error = criticRehearsalSamples[i]._outputs[0] - _critic.getOutput(0);

			squareError += error * error;

			FeedForwardNeuralNetwork::Gradient gradient;
			_critic.getGradientLinearOutputLayer(criticRehearsalSamples[i]._outputs, gradient);
			_critic.moveAlongGradientMomentum(gradient, _criticAlpha, _criticMomentum);
		}
	}

	_critic.decayWeights(_criticDecay);

	// Add sample
	EligibilityTrace trace;
	trace._inputs = _prevInputs;
	trace._value = criticTarget;
	trace._fitness = fitness;

	_eligibilityTraceChain.push_front(trace);

	while (_eligibilityTraceChain.size() > _maxEligibilityTraceChainSize)
		_eligibilityTraceChain.pop_back();

	float scaledError = error * _actorAlpha;

	if (scaledError < 0.0f)
		scaledError *= _negativeErrorMultiplier;

	if (std::abs(scaledError) < _minError)
		scaledError = -_minError;

	_actor.activateAndReinforce(scaledError);

	for (size_t i = 0; i < getNumInputs(); i++)
		_critic.setInput(i, currentInputs[i]);

	_critic.activateLinearOutputLayer();

	_prevValue = _critic.getOutput(0);

	for (size_t i = 0; i < getNumInputs(); i++)
		_prevInputs[i] = currentInputs[i];

	//_prevValue = newPrevValue;
}

void ActorCriticAgent::writeToStream(std::ostream &stream) {
	stream << _actorAlpha << " " << _criticAlpha << " " << _lambda << " " << _gamma << " " << _minError << std::endl;
	stream << _numBackpropPasses << " " << _criticMomentum << " " << _criticErrorTolerance << " " << _negativeErrorMultiplier << std::endl;
	stream << _numPseudoRehearsalSamplesCritic << " " << _pseudoRehearsalSampleStdDev << " " << _pseudoRehearsalSampleMean << std::endl;
	stream << _maxEligibilityTraceChainSize << std::endl;

	_actor.writeToStream(stream);
	_critic.writeToStream(stream);
}

void ActorCriticAgent::readFromStream(std::istream &stream) {
	stream >> _actorAlpha;
	stream >> _criticAlpha;
	stream >> _lambda;
	stream >> _gamma;
	stream >> _minError;

	stream >> _numBackpropPasses;
	stream >> _criticMomentum;
	stream >> _criticErrorTolerance;
	stream >> _negativeErrorMultiplier;

	stream >> _numPseudoRehearsalSamplesCritic;
	stream >> _pseudoRehearsalSampleStdDev;
	stream >> _pseudoRehearsalSampleMean;

	stream >> _maxEligibilityTraceChainSize;

	_actor.readFromStream(stream);
	_critic.readFromStream(stream);

	_prevInputs.assign(_actor.getNumInputs(), 0.0f);
}