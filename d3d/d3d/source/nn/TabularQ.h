#pragma once

#include <vector>
#include <random>

namespace nn {
	class TabularQ {
	private:
		size_t _numStates;
		size_t _numActions;

		std::vector<float> _q; // Dimension: state x action

		size_t _state;
		size_t _action;

		size_t _prevState;
		size_t _prevAction;

		float getQ(size_t s, size_t a) const {
			return _q[s + _numStates * a];
		}

		void setQ(size_t s, size_t a, float value) {
			_q[s + _numStates * a] = value;
		}

	public:
		std::mt19937 _generator;

		float _gamma;
		float _alpha;

		float _randomActionChance;

		TabularQ()
			: _prevState(0), _prevAction(0),
			_gamma(0.9f), _alpha(0.5f),
			_randomActionChance(0.2f)
		{}

		void create(size_t numStates, size_t numActions, unsigned long seed);

		void step(float fitness);

		void setState(size_t state) {
			_state = state;
		}

		size_t getAction() const {
			return _action;
		}

		size_t getNumStates() const {
			return _numStates;
		}

		size_t getNumActions() const {
			return _numActions;
		}
	};
}
