/*
NN
Copyright (C) 2013 Eric Laukien

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#include <nn/Cacla.h>
#include <algorithm>
#include <iostream>

using namespace nn;

Cacla::Cacla()
: _prevValue(0.0f), _variance(1.0f), _outputOffsetScalar(1.0f), _outputOffsetAbsErrorScalar(20.0f), _reverseOffsetErrorScalar(40.0f),
_actorAlpha(0.001f), _criticAlpha(0.001f), _actorDecay(0.005f), _criticDecay(0.005f),
_lambda(0.33f), _gamma(0.95f),
_numCriticBackpropPasses(8), _numActorBackpropPassesMultiplier(200.0f), _maxNumActorBackpropPasses(8),
_actorMomentum(0.2f), _criticMomentum(0.2f),
_varianceDecay(0.07f),
_actorErrorTolerance(0.01f),
_criticErrorTolerance(0.01f),
_numPseudoRehearsalSamplesActor(8),
_numPseudoRehearsalSamplesCritic(8),
_pseudoRehearsalSampleStdDev(0.75f),
_pseudoRehearsalSampleMean(0.0f),
_maxEligibilityTraceChainSize(60)
{}

void Cacla::createRandom(size_t numInputs, size_t numOutputs,
	size_t numActorHiddenLayers, size_t numActorNeuronsPerHiddenLayer,
	size_t numCriticHiddenLayers, size_t numCriticNeuronsPerHiddenLayer,
	const BrownianPerturbation &perturbation,
	float minWeight, float maxWeight,
	unsigned long seed)
{
	_actor.createRandom(numInputs, numOutputs, numActorHiddenLayers, numActorNeuronsPerHiddenLayer, minWeight, maxWeight, seed + 1);
	_critic.createRandom(numInputs, 1, numCriticHiddenLayers, numCriticNeuronsPerHiddenLayer, minWeight, maxWeight, seed + 2);

	_prevInputs.assign(numInputs, 0.0f);

	_outputOffsets.assign(numOutputs, perturbation);
	_outputs.assign(numOutputs, 0.0f);

	_generator.seed(seed);
}

void Cacla::step(float fitness, float dt) {
	std::vector<float> currentInputs(getNumInputs());

	for (size_t i = 0; i < getNumInputs(); i++)
		currentInputs[i] = _actor.getInput(i);

	_critic.activateLinearOutputLayer();

	float value = _critic.getOutput(0);

	// Gather rehearsal samples
	std::vector<IOSet> actorRehearsalSamples(_numPseudoRehearsalSamplesActor);
	std::vector<IOSet> criticRehearsalSamples(_numPseudoRehearsalSamplesCritic);

	std::normal_distribution<float> pseudoRehearsalInputDistribution(_pseudoRehearsalSampleMean, _pseudoRehearsalSampleStdDev);

	for (size_t i = 0; i < _numPseudoRehearsalSamplesActor; i++) {
		// Generate actor sample
		actorRehearsalSamples[i]._inputs.resize(_actor.getNumInputs());

		for (size_t j = 0; j < _actor.getNumInputs(); j++) {
			actorRehearsalSamples[i]._inputs[j] = pseudoRehearsalInputDistribution(_generator);
			_actor.setInput(j, actorRehearsalSamples[i]._inputs[j]);
		}

		_actor.activateLinearOutputLayer();

		actorRehearsalSamples[i]._outputs.resize(_actor.getNumOutputs());

		for (size_t j = 0; j < _actor.getNumOutputs(); j++)
			actorRehearsalSamples[i]._outputs[j] = _actor.getOutput(j);
	}

	for (size_t i = 0; i < _numPseudoRehearsalSamplesCritic; i++) {
		// Generate critic sample
		criticRehearsalSamples[i]._inputs.resize(_critic.getNumInputs());

		for (size_t j = 0; j < _actor.getNumInputs(); j++) {
			criticRehearsalSamples[i]._inputs[j] = pseudoRehearsalInputDistribution(_generator);
			_critic.setInput(j, criticRehearsalSamples[i]._inputs[j]);
		}

		_critic.activateLinearOutputLayer();

		criticRehearsalSamples[i]._outputs.resize(_critic.getNumOutputs());

		for (size_t j = 0; j < _critic.getNumOutputs(); j++)
			criticRehearsalSamples[i]._outputs[j] = _critic.getOutput(j);
	}

	float newPrevValue = fitness + _gamma * value;
	float tdError = newPrevValue - _prevValue;
	float criticTarget = _prevValue + _lambda * tdError;

	// Add discounted reward to eligibility traces. Propagate reward downward
	float prevChainValue = criticTarget;

	for (std::list<EligibilityTrace>::iterator it = _eligibilityTraceChain.begin(); it != _eligibilityTraceChain.end(); it++) {
		it->_value = (1.0f - _lambda) * it->_value + _lambda * (it->_fitness + _gamma * prevChainValue);
		prevChainValue = it->_value;
	}

	// Track square error for early stopping
	float squareError = 99999.0f;

	for (size_t p = 0; p < _numCriticBackpropPasses && squareError > _criticErrorTolerance; p++) {
		float error;

		// Recalculate sum of square errors
		squareError = 0.0f;

		// Train on new sample
		for (size_t i = 0; i < _prevInputs.size(); i++)
			_critic.setInput(i, _prevInputs[i]);

		_critic.activateLinearOutputLayer();

		error = criticTarget - _critic.getOutput(0);

		squareError += error * error;

		FeedForwardNeuralNetwork::Gradient gradient;
		_critic.getGradientLinearOutputLayer(std::vector<float>(1, criticTarget), gradient);
		_critic.moveAlongGradientMomentum(gradient, _criticAlpha, _criticMomentum);

		// Keep value for next state
		/*for (size_t i = 0; i < currentInputs.size(); i++)
			_critic.setInput(i, currentInputs[i]);

		_critic.activateLinearOutputLayer();

		_critic.getGradientLinearOutputLayer(std::vector<float>(1, value), gradient);
		_critic.moveAlongGradientMomentum(gradient, _criticAlpha, _criticMomentum);*/

		// Train on previous samples
		for (std::list<EligibilityTrace>::iterator it = _eligibilityTraceChain.begin(); it != _eligibilityTraceChain.end(); it++) {
			for (size_t i = 0; i < it->_inputs.size(); i++)
				_critic.setInput(i, it->_inputs[i]);

			_critic.activateLinearOutputLayer();

			error = it->_value - _critic.getOutput(0);

			squareError += error * error;

			FeedForwardNeuralNetwork::Gradient gradient;
			_critic.getGradientLinearOutputLayer(std::vector<float>(1, it->_value), gradient);
			_critic.moveAlongGradientMomentum(gradient, _criticAlpha, _criticMomentum);
		}

		// Train on rehearsal samples
		for (size_t i = 0; i < criticRehearsalSamples.size(); i++) {
			for (size_t j = 0; j < _critic.getNumInputs(); j++)
				_critic.setInput(j, criticRehearsalSamples[i]._inputs[j]);

			_critic.activateLinearOutputLayer();

			error = criticRehearsalSamples[i]._outputs[0] - _critic.getOutput(0);

			squareError += error * error;

			FeedForwardNeuralNetwork::Gradient gradient;
			_critic.getGradientLinearOutputLayer(criticRehearsalSamples[i]._outputs, gradient);
			_critic.moveAlongGradientMomentum(gradient, _criticAlpha, _criticMomentum);
		}
	}

	_critic.decayWeights(_criticDecay);

	// Add sample
	EligibilityTrace trace;
	trace._inputs = _prevInputs;
	trace._value = criticTarget;
	trace._fitness = fitness;

	_eligibilityTraceChain.push_front(trace);

	while (_eligibilityTraceChain.size() > _maxEligibilityTraceChainSize)
		_eligibilityTraceChain.pop_back();

	// Update actor
	if (tdError > 0.0f) {
		size_t maxNumPasses = _maxNumActorBackpropPasses;// std::min(_maxNumActorBackpropPasses, static_cast<size_t>(std::ceilf(_numActorBackpropPassesMultiplier * tdError / std::sqrtf(_variance))));
		
		squareError = 99999.0f;

		for (size_t p = 0; p < maxNumPasses && squareError > _actorErrorTolerance; p++) {
			// Recalculate sum of square errors
			squareError = 0.0f;

			// Train on new sample
			for (size_t i = 0; i < _prevInputs.size(); i++)
				_actor.setInput(i, _prevInputs[i]);

			_actor.activateLinearOutputLayer();

			for (size_t i = 0; i < _actor.getNumOutputs(); i++) {
				float error = _outputs[i] - _actor.getOutput(i);

				squareError += error * error;
			}

			FeedForwardNeuralNetwork::Gradient gradient;
			_actor.getGradientLinearOutputLayer(_outputs, gradient);
			_actor.moveAlongGradientMomentum(gradient, _actorAlpha, _actorMomentum);

			// Train on rehearsal samples
			for (size_t i = 0; i < actorRehearsalSamples.size(); i++) {
				for (size_t j = 0; j < _actor.getNumInputs(); j++)
					_actor.setInput(j, actorRehearsalSamples[i]._inputs[j]);

				_actor.activateLinearOutputLayer();

				for (size_t j = 0; j < _actor.getNumOutputs(); j++) {
					float error = _outputs[j] - _actor.getOutput(j);

					squareError += error * error;
				}

				FeedForwardNeuralNetwork::Gradient gradient;
				_actor.getGradientLinearOutputLayer(actorRehearsalSamples[i]._outputs, gradient);
				_actor.moveAlongGradientMomentum(gradient, _actorAlpha, _actorMomentum);
			}
		}
	}/* else {
		std::vector<float> randomOffsetOutputs(getNumOutputs());

		for (size_t i = 0; i < randomOffsetOutputs.size(); i++)
			randomOffsetOutputs[i] = std::max(-1.0f, std::min(1.0f, _outputs[i] - (1.0f - tdError * _reverseOffsetErrorScalar) * _outputOffsets[i]._position));

		for (size_t p = 0; p < _maxNumActorBackpropPasses; p++) {
			// Train on new sample
			for (size_t i = 0; i < _prevInputs.size(); i++)
				_actor.setInput(i, _prevInputs[i]);

			_actor.activateLinearOutputLayer();

			FeedForwardNeuralNetwork::Gradient gradient;
			_actor.getGradientLinearOutputLayer(randomOffsetOutputs, gradient);
			_actor.moveAlongGradientMomentum(gradient, _actorAlpha, _actorMomentum);

			// Train on rehearsal samples
			for (size_t i = 0; i < actorRehearsalSamples.size(); i++) {
				for (size_t j = 0; j < _actor.getNumInputs(); j++)
					_actor.setInput(j, actorRehearsalSamples[i]._inputs[j]);

				_actor.activateLinearOutputLayer();

				FeedForwardNeuralNetwork::Gradient gradient;
				_actor.getGradientLinearOutputLayer(actorRehearsalSamples[i]._outputs, gradient);
				_actor.moveAlongGradientMomentum(gradient, _actorAlpha, _actorMomentum);
			}
		}
	}*/

	_actor.decayWeights(_actorDecay);

	for (size_t i = 0; i < currentInputs.size(); i++)
		_actor.setInput(i, currentInputs[i]);

	_actor.activateLinearOutputLayer();

	float randomIncrease = tdError < 0.0f ? -tdError * _outputOffsetAbsErrorScalar : 0.0f;

	for (size_t i = 0; i < getNumOutputs(); i++) {
		_outputOffsets[i].update(_generator, dt);
		_outputs[i] = std::max(-1.0f, std::min(1.0f, _actor.getOutput(i) + _outputOffsets[i]._position * (_outputOffsetScalar + randomIncrease)));
	}

	for (size_t i = 0; i < currentInputs.size(); i++)
		_critic.setInput(i, currentInputs[i]);

	_critic.activateLinearOutputLayer();

	_prevValue = _critic.getOutput(0);

	for (size_t i = 0; i < getNumInputs(); i++)
		_prevInputs[i] = currentInputs[i];

	_variance = (1.0f - _varianceDecay) * _variance + _varianceDecay * tdError * tdError;
}

void Cacla::writeToStream(std::ostream &stream) {
	stream << _outputOffsetScalar << std::endl;
	stream << _actorAlpha << " " << _criticAlpha << " " << _lambda << " " << _gamma << std::endl;
	stream << _numCriticBackpropPasses << " " << _numActorBackpropPassesMultiplier << " " << _maxNumActorBackpropPasses << std::endl;
	stream << _actorMomentum << " " << _criticMomentum << std::endl;
	stream << _varianceDecay << std::endl;
	stream << _numPseudoRehearsalSamplesActor << std::endl;
	stream << _numPseudoRehearsalSamplesCritic << std::endl;
	stream << _pseudoRehearsalSampleStdDev << std::endl;
	stream << _pseudoRehearsalSampleMean << std::endl;

	_actor.writeToStream(stream);
	_critic.writeToStream(stream);
}

void Cacla::readFromStream(std::istream &stream) {
	stream >> _outputOffsetScalar;

	stream >> _actorAlpha;
	stream >> _criticAlpha;
	stream >> _lambda;
	stream >> _gamma;

	stream >> _numCriticBackpropPasses;
	stream >> _numActorBackpropPassesMultiplier;
	stream >> _maxNumActorBackpropPasses;

	stream >> _actorMomentum;
	stream >> _criticMomentum;

	stream >> _varianceDecay;

	stream >> _numPseudoRehearsalSamplesActor;
	stream >> _numPseudoRehearsalSamplesCritic;
	stream >> _pseudoRehearsalSampleStdDev;
	stream >> _pseudoRehearsalSampleMean;

	_actor.readFromStream(stream);
	_critic.readFromStream(stream);

	_prevInputs.assign(_actor.getNumInputs(), 0.0f);

	_outputOffsets.assign(_actor.getNumOutputs(), nn::BrownianPerturbation());

	_outputs.assign(_actor.getNumOutputs(), 0.0f);
}