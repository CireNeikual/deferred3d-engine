#pragma once

#include <LuaContext.hpp>

#include <boost/variant.hpp>

#include <list>

namespace d3d {
	typedef boost::make_recursive_variant<std::string, double, bool,
		std::vector<std::pair<
		boost::variant<std::string, double, bool>,
		boost::recursive_variant_>>>::type
		LuaAnyValue;

	class LuaWorker {
	public:
		LuaContext _context;
	};
}