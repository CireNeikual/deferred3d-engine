#pragma once

#include <scene/SceneObject.h>

#include <lua/LuaWorker.h>

namespace d3d {
	class LuaSceneObject {
	private:
		LuaAnyValue _data;

	public:
		void create();

		// Inherited from SceneObject
		void updateWithThreadData(float dt, size_t threadIndex);

		static void bind(LuaContext &context);
	};
}