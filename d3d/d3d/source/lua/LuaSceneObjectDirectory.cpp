#include <lua/LuaSceneObjectDirectory.h>

#include <fstream>
#include <iostream>

using namespace d3d;

bool LuaSceneObjectDirectory::createAsset(const std::string &name) {
	std::ifstream fromFile(name);

	if (!fromFile.is_open()) {
#ifdef D3D_DEBUG
		std::cerr << "Error: Could not open directory file " << name << std::endl;
#endif
		return false;
	}

	while (!fromFile.eof() && fromFile.good()) {
		std::string fileName;
		std::string className;
	}

	return true;
}