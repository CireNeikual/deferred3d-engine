#pragma once

#include <assetmanager/Asset.h>
#include <lua/LuaSceneObject.h>

#include <unordered_map>

namespace d3d {
	class LuaSceneObjectDirectory : public Asset {
	private:
		struct LuaSceneObjectData {
			std::function<LuaAnyValue()> _init;
			std::function<LuaAnyValue(LuaAnyValue)> _onQueue;
			std::function<LuaAnyValue(LuaAnyValue)> _onAdd;
			std::function<LuaAnyValue(float, LuaAnyValue)> _update;
		};

		std::unordered_map<std::string, std::unique_ptr<LuaSceneObjectData>> _nameToSceneObjectData;

	public:
		// Inherited from Asset
		bool createAsset(const std::string &name);
	};
}